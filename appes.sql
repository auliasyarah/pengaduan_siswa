/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.5.9-MariaDB : Database - appes
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `guru` */

DROP TABLE IF EXISTS `guru`;

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(225) NOT NULL,
  `kode_guru` varchar(225) NOT NULL,
  `nama_guru` varchar(225) NOT NULL,
  `no_telp` varchar(225) NOT NULL,
  PRIMARY KEY (`id_guru`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `guru` */

insert  into `guru`(`id_guru`,`id_user`,`kode_guru`,`nama_guru`,`no_telp`) values 
(1,'10','1121','Rere Hening','08154421452226'),
(2'18','303555','Yanti Rahayunigrum','085676812345');

/*Table structure for table `pelanggaran` */

DROP TABLE IF EXISTS `pelanggaran`;

CREATE TABLE `pelanggaran` (
  `id_pelanggaran` int(225) NOT NULL AUTO_INCREMENT,
  `pelanggaran` text DEFAULT NULL,
  `poin` int(225) DEFAULT NULL,
  PRIMARY KEY (`id_pelanggaran`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `pelanggaran` */

insert  into `pelanggaran`(`id_pelanggaran`,`pelanggaran`,`poin`) values 
(1,'Merokok Dd Dalam Kelas',200);
(2,'Membawa Sajam di Area Sekolah',200);
(3,'Memakai Seragam Tidak Sesuai Ketentuan',3);
(4,'Membawa Perlengkapan Make Up',10);
(5,'Tatanan Rambut Tidak Sesuai Ketentuan',5);
(6,'Datang Terlambat Tanpa Alasan',3);
(7,'Tidak Masuk Sekolah Tanpa Keterangan',8);
(8,'Meninggalkan Pelajaran Tanpa Ijin (Membolos)',10);
(9,'Bertato',15);
(10,'Berkelahi di Area Sekolah Maupun Luar Sekolah Saat Mengenakan Seragam Sekolah',200);
(11,'Melakukan Bullying',100);
(12,'Melakukan Tindakan Asusila',800);
/*Table structure for table `pengaduan` */

DROP TABLE IF EXISTS `pengaduan`;

CREATE TABLE `pengaduan` (
  `id_pengaduan` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(225) NOT NULL,
  `nama_pengaduan` varchar(225) NOT NULL,
  `deskripsi` text NOT NULL,
  `tanggal` datetime NOT NULL,
  `status_pengaduan` varchar(225) NOT NULL,
  `foto` varchar(225) NOT NULL,
  `id_pelanggaran` int(225) NOT NULL DEFAULT 1,
  `terlapor` varchar(225) NOT NULL,
  PRIMARY KEY (`id_pengaduan`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengaduan` */

insert  into `pengaduan`(`id_pengaduan`,`id_user`,`nama_pengaduan`,`deskripsi`,`tanggal`,`status_pengaduan`,`foto`,`id_pelanggaran`,`terlapor`) values 
(13,'10','test','test','2022-05-20 10:11:51','1','',5,'1');

/*Table structure for table `siswa` */

DROP TABLE IF EXISTS `siswa`;

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nis` varchar(225) NOT NULL,
  `nama_siswa` varchar(225) NOT NULL,
  `kelas` varchar(225) DEFAULT NULL,
  `no_telp` varchar(225) NOT NULL,
  `email` varchar(225) DEFAULT NULL,
  `no_induk` varchar(225) DEFAULT NULL,
  `poin_siswa` int(225) DEFAULT 0,
  PRIMARY KEY (`id_siswa`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `siswa` */

insert  into `siswa`(`id_siswa`,`id_user`,`nis`,`nama_siswa`,`kelas`,`no_telp`,`email`,`no_induk`,`poin_siswa`) values 
(1,4,'14023','Firda Salsabila','X RPL','081425411441','firsalbila@gmail.com',183069113,-200);
(2,5,'140111','Ninda Rahayuningrum','XI Mekatronika 2','081276610918','nindaaryu@gmail.com',152768991,0);

/*Table structure for table `tanggapan` */

DROP TABLE IF EXISTS `tanggapan`;

CREATE TABLE `tanggapan` (
  `id_tanggapan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengaduan` varchar(225) NOT NULL,
  `id_user` varchar(225) NOT NULL,
  `tanggapan` text NOT NULL,
  `tanggal_tanggapan` datetime NOT NULL,
  PRIMARY KEY (`id_tanggapan`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tanggapan` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id_user`,`username`,`password`,`status`) values 
(1,'admin010','admin010','admin'),
(4,'firdasalsabila','1234','siswa'),
(5, 'nindarahayu', 'nindarahayu5', 'siswa')
(10,'rerening1','guru1','guru'),
(18,'yanti2','guru2','guru');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
